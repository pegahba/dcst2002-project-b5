## How to use

### Frontpage

![Frontpage image](images/Screenshot_2021-11-22_092840.png)

The frontpage is the default page when navigating to our site.

It consists of a navigation bar at the top, and a page of suggested games to investigate. The navbar
is a simple and easy way to find what you seek. There is a general list of all games in the site
database, a search function for these games, and a account page where you can sign in or create a
user.

The page itself consists of a dynamic list of games defined by user activity. The listing here will
be a result of the games which have recently updated, recieved most attention, or are currently the
most high rated.

### All games

![All games page](images/Screenshot_2021-11-22_100803.png)

A handy overview over registered games on the website. Press those cool page buttons.

### Gamepage

![Gamepage image](images/Screenshot_2021-11-22_093348.png)

The gamepage is a more detailed look at each game, and also the place to navigate to similar games,
read and rate reviews, or write your own review.

Should you not be logged in, the "leave a review" portion will be replaced with a login suggestion,
which will navigate to the login page.

### Search Function

![Search function image](images/Screenshot_2021-11-20_133603.png)

noticed this? It allows you to search for existing game in the database

![Searched image](images/Screenshot_2021-11-20_141914.png)

Even with incomplete searches!

#### Genres and Platforms

![Genre/platform image](images/Screenshot_2021-11-22_093623.png)

These tabs? They aren't just for show. They are buttons to search for the named genres and
platforms!

![Genre/platform image2](images/Screenshot_2021-11-22_093737.png)

A handy notice which genre is searched.

![Genre/platform image3](images/Screenshot_2021-11-20_134114.png)

never get lost!

### Adding games

![SearchnoFound image](images/Screenshot_2021-11-20_135310.png)

If you can't find a game in the database, you may add it yourself by clicking the "Can't find your
game bar?..." bar in the image.

![Add game image](images/Screenshot_2021-11-20_135600.png)

Just fill out the relevant parts with info and add a nice picturelink, then you're good to go.

### Login page

![loginpage image](images/Screenshot_2021-11-22_101336.png)

If you have a user, just pop those creds in. Otherwise head to the creation page.

### User Creation

![User creation image](images/Screenshot_2021-11-22_101026.png)

New to the site? Just stop on by and get yourself registered.

### Userpage

![Userpage image](images/Screenshot_2021-11-22_094931.png)

Here all user's reviews will be gathered in one place, with the total upvotes of each review. For
the logged in user, they will be able to edit or delete their reviews from this page with the help
of the buttons attached to each review. These buttons will not be visible on other users pages. On
your own userpage you will also find the logout button.

### Editpage

![Editpage image](images/Screenshot_2021-11-22_094206.png)

Here you will have the opportunity to change your existing review and rating. The form will be
prefilled with the existing review and rating. To save the changes, hit the "save changes" button.

### Userpage edit

![Useredit](images/Screenshot_2021-11-22_094548.png)

Edit your profile! The picture will show immediately if it works.(only imagelinks)
