# How to install and use this application

This project is cloned from https://gitlab.com/ntnu-dcst2002/todo-client-tests, and will generally
be installed and operated in the same manner.

## Install and start service

### Install node.js and gitbash.

![nodejs](images/Screenshot_2021-11-21_164425.png)

1. Press the prefered option
2. Download should start automatically, accept download
3. Go through setup

   ![gitbash](images/Screenshot_2021-11-21_164708.png)

4. Press the prefered OS
5. Download should start automatically, accept download
6. Go through setup

### Clone or unpack repo

- The repo can be unzipped in the wanted folder.
- The repo can be cloned either through the Git Bash terminal, or with the help of an IDE like
  VSCode.

![git bash terminal](images/Screenshot_2021-11-21_165937.png)

1. Open the Git Bash terminal
2. navigate to location where you want to store the repo

   ![clone link](images/Screenshot_2021-11-21_154905.png)

3. Retrieve Gitlab https clone link

   ![clone terminal](images/Screenshot_2021-11-21_171135.png)

4. write "git clone url-address"

### Installing application

- There are two variations to install:
- **Option 1: navigate entirely in terminal.**

  ![terminal client](images/Screenshot_2021-11-21_164031.png)

1. Open a terminal (gitbash)

   ![terminal install client](images/Screenshot_2021-11-21_164128.png)

2. Navigate to the "client" folder in the repo

3. write "npm install"
4. If installation was successful, write "npm start"

   ![terminal server](images/Screenshot_2021-11-21_164031.png)

5. open a new terminal (don't close the old one)
6. Navigate back into the main repo folder, then enter the "server" folder

   ![terminal install server](images/Screenshot_2021-11-21_164221.png)

7. write "npm install"
8. If installation was successful, write "npm start"

- **Option 2:**

1. Search for folder on computer or use file explorer to locate repo

   ![right click gitbash client](images/Screenshot_2021-11-21_155314.png)

2. right click on client folder and select "Git Bash here"

   ![Git bash install and run](images/Screenshot_2021-11-21_155106.png)

3. Write "npm install", and "npm start" if install was successful

4. Don't close the terminal window

   ![right click gitbash server](images/Screenshot_2021-11-21_155236.png)

5. In repo folder, right clicker server and select "Git Bash here"

   ![git bash install and run](images/Screenshot_2021-11-21_155434.png)

6. Write "npm install", and "npm start" if install was successful
7. Don't close the terminal window

Congratulations! You have successfully installed and run the web application To access the
application:

1. Go to your preferred web browser.
2. enter "localhost:3000"
3. do stuff
