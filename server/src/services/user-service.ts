import { response } from 'express';
import pool from '../mysql-pool';

export type User = {
  username: string;
};

export type UserReview = {
  review_id: number;
  review: string;
  username: string;
  game_rating: number;
  game_id: number;
  title: string;
  release_year: number;
  photo: string;
  rating: number;
  modified_time: string;
};

export type UserProfile = {
  profilePicture: string;
  bios: string;
};

class UserService {
  logIn(username: string, pass: string) {
    return new Promise<User | undefined>((resolve, reject) => {
      pool.query(
        'SELECT username FROM users WHERE username=? AND password=?',
        [username, pass],
        (error, results) => {
          if (error) return reject(error);

          resolve(results[0]);
        }
      );
    });
  }

  getUserReviews(username: string) {
    return new Promise<UserReview[]>((resolve, reject) => {
      pool.query(
        'SELECT r.*, SUM(review_rating.review_rating)"rating", g.title, g.release_year, g.photo FROM review r JOIN game g ON r.game_id=g.game_id AND r.username=? LEFT JOIN review_rating ON review_rating.review_id=r.review_id GROUP BY review_id',
        [username],
        (error, results) => {
          if (error) return reject(error);
          resolve(results);
        }
      );
    });
  }

  addUser(username: string, password: string) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'INSERT INTO users SET username=?, password=?',
        [username, password],
        (error, _results) => {
          if (error) return reject(error);

          resolve();
        }
      );
    });
  }

  getUser(username: string) {
    return new Promise<UserProfile | undefined>((resolve, reject) => {
      pool.query(
        'SELECT profilePicture, bios FROM users WHERE username=?',
        [username],
        (error, results) => {
          if (error) return reject(error);

          resolve(results[0]);
        }
      );
    });
  }

  editUser(profilePicture: string, bios: string, username: string) {
    return new Promise<UserProfile>((resolve, reject) => {
      pool.query(
        'UPDATE users SET profilePicture=?, bios=? WHERE username=?',
        [profilePicture, bios, username],
        (error, results) => {
          if (error) return reject(error);

          resolve(results);
        }
      );
    });
  }
}

const userService = new UserService();
export default userService;
