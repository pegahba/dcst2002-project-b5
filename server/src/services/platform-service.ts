import { resolvePlugin } from '@babel/core';
import pool from '../mysql-pool';
import { Game } from './game-service';

export type Platform = {
  game_id: number;
  platform: string;
};

class PlatformService {
  getPlatforms(id: number) {
    return new Promise<Platform[]>((resolve, reject) => {
      pool.query('SELECT * FROM platforms WHERE game_id = ?', [id], (error, results) => {
        if (error) return reject(error);

        resolve(results);
      });
    });
  }

  getPlatformList(platform: string) {
    return new Promise<Game[]>((resolve, reject) => {
      pool.query(
        'SELECT game.* FROM game JOIN platforms ON game.game_id=platforms.game_id WHERE platforms.platform = ?',
        [platform],
        (error, results) => {
          if (error) return reject(error);

          resolve(results);
        }
      );
    });
  }

  getAllPlatforms() {
    return new Promise<Platform[]>((resolve, reject) => {
      pool.query(
        'SELECT game_id,GROUP_CONCAT(platform SEPARATOR ", ")"platform" FROM platforms GROUP BY game_id',
        (error, results) => {
          if (error) return reject(error);

          resolve(results);
        }
      );
    });
  }

  addPlatforms(game_id: number, platforms: []) {
    return new Promise<Platform[]>((resolve, reject) => {
      platforms.map((platform) => { platform != '' ? 
        pool.query('INSERT INTO platforms SET game_id=?, platform=?', [game_id, platform],
        (error, results) => {
          if (error) return reject(error);

          resolve(results);
        }) : ''
      })
    })
  }
}

const platformService = new PlatformService();
export default platformService;
