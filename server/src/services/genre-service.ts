import pool from '../mysql-pool';
import { Game } from './game-service';

export type Genre = {
  game_id: number;
  genre: string;
};

class GenreService {
  getGenres(id: number) {
    return new Promise<Genre[]>((resolve, reject) => {
      pool.query('SELECT * FROM genre WHERE game_id = ?', [id], (error, results) => {
        if (error) return reject(error);

        resolve(results);
      });
    });
  }

  getGenreList(genre: string) {
    return new Promise<Game[]>((resolve, reject) => {
      pool.query(
        'SELECT game.* FROM game JOIN genre ON game.game_id=genre.game_id WHERE genre.genre = ?',
        [genre],
        (error, results) => {
          if (error) return reject(error);

          resolve(results);
        }
      );
    });
  }

  getAllGenres() {
    return new Promise<Genre[]>((resolve, reject) => {
      pool.query(
        'SELECT game_id,GROUP_CONCAT(genre SEPARATOR ", ")"genre" FROM genre GROUP BY game_id',
        (error, results) => {
          if (error) return reject(error);

          resolve(results);
        }
      );
    });
  }

  addGenres(game_id: number, genres: []) {
    return new Promise<Genre[]>((resolve, reject) => {
      genres.map((genre) => { genre != '' ?
        pool.query('INSERT INTO genre SET game_id=?, genre=?', [game_id, genre],
        (error, results) => {
          if (error) return reject(error);

          resolve(results);
        }) : ''
      })
    })
  }
}

const genreService = new GenreService();
export default genreService;
