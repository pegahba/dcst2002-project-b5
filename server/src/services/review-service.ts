import pool from '../mysql-pool';

export type Review = {
  review_id: number;
  rating: number;
  review: string;
  username: string;
  game_rating: number;
};

export type GameRating = {
  avg_rating: number;
};

export type ReviewToEdit = {
  review_id: number;
  review: string;
  game_rating: number;
  title: string;
  release_year: number;
  photo: string;
};

class ReviewService {
  getReviews(id: number) {
    return new Promise<Review[]>((resolve, reject) => {
      pool.query(
        'SELECT review.*, SUM(review_rating.review_rating)"rating" FROM review LEFT JOIN review_rating ON review_rating.review_id=review.review_id WHERE game_id = ? GROUP by review_id ORDER BY review.modified_time DESC',
        [id],
        (error, results) => {
          if (error) return reject(error);

          resolve(results);
        }
      );
    });
  }

  getReview(review_id: number) {
    return new Promise<ReviewToEdit>((resolve, reject) => {
      pool.query(
        'SELECT review.*, g.title, g.release_year, g.photo FROM review JOIN game g ON (review.game_id = g.game_id) WHERE review.review_id = ?',
        [review_id],
        (error, results) => {
          if (error) return reject(error);

          resolve(results[0]);
        }
      );
    });
  }

  getRating(id: number) {
    return new Promise<GameRating>((resolve, reject) => {
      pool.query(
        'SELECT ROUND(AVG(game_rating), 2) AS "avg_rating" FROM `review` WHERE game_id=?',
        [id],
        (error, results) => {
          if (error) return reject(error);

          resolve(results[0]);
        }
      );
    });
  }

  rateReview(id: number, username: string) {
    return new Promise<number>((resolve, reject) => {
      pool.query(
        'INSERT INTO review_rating SET review_rating=1, review_id=?, username=?',
        [id, username],
        (error, results) => {
          if (error) return reject(error);

          resolve(results);
        }
      );
    });
  }

  addReview(rating: number, review: string, username: string, game_id: number) {
    return new Promise<number>((resolve, reject) => {
      if (rating !== 0) {
        pool.query(
          'INSERT INTO review SET username =?, review=?, game_rating=?, game_id=?',
          [username, review, rating, game_id],
          (error, results) => {
            if (error) return reject(error);

            resolve(Number(results.insertId));
          }
        );
      } else {
        pool.query(
          'INSERT INTO review SET username =?, review=?, game_rating=NULL, game_id=?',
          [username, review, game_id],
          (error, results) => {
            if (error) return reject(error);

            resolve(Number(results.insertId));
          }
        );
      }
    });
  }

  deleteReview(id: number) {
    return new Promise<void>((resolve, reject) => {
      pool.query('DELETE FROM review WHERE review_id=?', [id], (error, _results) => {
        if (error) return reject(error);

        resolve();
      });
    });
  }

  updateReview(review_id: number, review: string, game_rating: number) {
    return new Promise<number>((resolve, reject) => {
      if (game_rating !== 0) {
        pool.query(
          'UPDATE review SET review = ?, game_rating = ? WHERE review_id = ?',
          [review, game_rating, review_id],
          (error, results) => {
            if (error) return reject(error);

            resolve(results);
          }
        );
      } else {
        pool.query(
          'UPDATE review SET review = ?, game_rating = NULL WHERE review_id = ?',
          [review, review_id],
          (error, results) => {
            if (error) return reject(error);

            resolve(results);
          }
        );
      }
    });
  }
}

const reviewService = new ReviewService();
export default reviewService;
