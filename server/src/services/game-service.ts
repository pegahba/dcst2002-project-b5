import pool from '../mysql-pool';

export type Game = {
  game_id: number;
  title: string;
  release_year: number;
  photo: string;
  description: string;
  modified_time: string;
};

class GameService {
  get(id: number) {
    return new Promise<Game | undefined>((resolve, reject) => {
      pool.query('SELECT * FROM game WHERE game_id = ?', [id], (error, results) => {
        if (error) return reject(error);

        resolve(results[0]);
      });
    });
  }

  getGamePages(page: number) {
    return new Promise<Game[]>((resolve, reject) => {
      pool.query('SELECT * FROM game LIMIT 30 OFFSET ?', [page], (error, results) => {
        if (error) return reject(error);

        resolve(results);
      });
    });
  }

  getMostReviews() {
    return new Promise<Game[]>((resolve, reject) => {
      pool.query(
        'SELECT game.*, COUNT(review.game_id)"reviews" FROM game JOIN review ON game.game_id=review.game_id GROUP by game_id ORDER BY reviews desc limit 6',
        (error, results) => {
          if (error) return reject(error);

          resolve(results);
        }
      );
    });
  }

  getTopRated() {
    return new Promise<Game[]>((resolve, reject) => {
      pool.query(
        'SELECT game.*, ROUND(AVG(game_rating), 2) AS "avg_rating" FROM game JOIN review ON game.game_id=review.game_id GROUP by game_id ORDER BY avg_rating desc limit 6',
        (error, results) => {
          if (error) return reject(error);

          resolve(results);
        }
      );
    });
  }

  getLastReviewed() {
    return new Promise<Game[]>((resolve, reject) => {
      pool.query(
        'SELECT game.*, MAX(review.modified_time)"reviews" FROM game JOIN review ON game.game_id=review.game_id GROUP by game_id ORDER BY reviews desc limit 6',
        (error, results) => {
          if (error) return reject(error);

          resolve(results);
        }
      );
    });
  }

  addGame(title: string, release_year: number, photo: string, description: string) {
    return new Promise<number>((resolve, reject) => {
      pool.query(
        'INSERT INTO game SET title=?, release_year=?, photo=?, description=?',
        [title, release_year, photo, description],
        (error, results) => {
          if (error) return reject(error);

          resolve(Number(results.insertId));
        }
      );
    });
  }
}

const gameService = new GameService();
export default gameService;
