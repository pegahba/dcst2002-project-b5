import pool from '../mysql-pool';
import { Game } from './game-service';

class SearchService {
  getGame(title: string) {
    return new Promise<Game[]>((resolve, reject) => {
      pool.query('SELECT * FROM game WHERE title LIKE ?', [title], (error, results) => {
        if (error) return reject(error);

        resolve(results);
      });
    });
  }
}

const searchService = new SearchService();
export default searchService;
