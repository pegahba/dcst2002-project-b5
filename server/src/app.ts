import express from 'express';
import gameRouter from './routers/game-router';
import genreRouter from './routers/genre-router';
import platformRouter from './routers/platform-router';
import reviewRouter from './routers/review-router';
import searchRouter from './routers/search-router';
import userRouter from './routers/user-router';

const app = express();

app.use(express.json());
app.use('/api/v2', gameRouter, genreRouter, platformRouter, reviewRouter, userRouter, searchRouter);

export default app;
