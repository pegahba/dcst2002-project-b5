import express, { request, response } from 'express';
import platformService from '../services/platform-service';

const platformRouter = express.Router();

platformRouter.get('/games/:id/platforms', (request, response) => {
  const id = Number(request.params.id);
  platformService
    .getPlatforms(id)
    .then((rows) => response.send(rows))
    .catch((error) => response.status(500).send(error));
});

platformRouter.get('/platforms/:platform', (request, response) => {
  const platform = String(request.params.platform);
  platformService
    .getPlatformList(platform)
    .then((rows) => response.send(rows))
    .catch((error) => response.status(500).send(error));
});

platformRouter.get('/platforms', (_request, response) => {
  platformService
    .getAllPlatforms()
    .then((rows) => response.send(rows))
    .catch((error) => response.status(500).send(error));
});

platformRouter.post('/new/platforms', (request, response) => {
  const data = request.body;
  if (data && data.game_id.length != 0)
    platformService
      .addPlatforms(data.game_id, data.platforms)
      .then((_rows) => response.status(200))
      .catch((error) => response.status(500).send(error));
});

export default platformRouter;
