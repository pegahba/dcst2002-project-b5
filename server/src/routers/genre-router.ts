import express, { request, response } from 'express';
import genreService from '../services/genre-service';

const genreRouter = express.Router();

genreRouter.get('/games/:id/genres', (request, response) => {
  const id = Number(request.params.id);
  genreService
    .getGenres(id)
    .then((rows) => response.send(rows))
    .catch((error) => response.status(500).send(error));
});

genreRouter.get('/genres/:genre', (request, response) => {
  const genre = String(request.params.genre);
  genreService
    .getGenreList(genre)
    .then((rows) => response.send(rows))
    .catch((error) => response.status(500).send(error));
});

genreRouter.get('/genres', (_request, response) => {
  genreService
    .getAllGenres()
    .then((rows) => response.send(rows))
    .catch((error) => response.status(500).send(error));
});

genreRouter.post('/new/genres', (request, response) => {
  const data = request.body;
  if (data && data.game_id.length != 0)
    genreService
      .addGenres(data.game_id, data.genres)
      .then((_rows) => response.status(200))
      .catch((error) => response.status(500).send(error));
});

export default genreRouter;
