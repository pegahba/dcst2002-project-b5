import express, { request, response } from 'express';
import reviewService from '../services/review-service';

const reviewRouter = express.Router();

reviewRouter.get('/games/:id/reviews', (request, response) => {
  const id = Number(request.params.id);
  reviewService
    .getReviews(id)
    .then((rows) => response.send(rows))
    .catch((error) => response.status(500).send(error));
});

reviewRouter.get('/games/:id/rating', (request, response) => {
  const id = Number(request.params.id);
  reviewService
    .getRating(id)
    .then((rating) =>
      rating ? response.send(rating) : response.status(404).send('Rating not found')
    )
    .catch((error) => response.status(500).send(error));
});

reviewRouter.get('/review/:review_id/edit', (request, response) => {
  const id = Number(request.params.review_id);
  reviewService
    .getReview(id)
    .then((review) => response.send(review))
    .catch((error) => response.status(500).send(error));
});

reviewRouter.post('/review/rating', (request, response) => {
  const data = request.body;
  if (data && data.username.length != 0)
    reviewService
      .rateReview(data.id, data.username)
      .then(() => response.status(200))
      .catch((error) => response.status(500).send(error));
});

reviewRouter.post('/review/new', (request, response) => {
  const data = request.body;
  if (data && data.review.length != 0 && data.username.length != 0 && data.game_id.length != 0)
    reviewService
      .addReview(data.rating, data.review, data.username, data.game_id)
      .then(() => response.status(200))
      .catch((error) => response.status(500).send(error));
});

reviewRouter.delete('/reviews/:id', (request, response) => {
  reviewService
    .deleteReview(Number(request.params.id))
    .then((_result) => response.status(200))
    .catch((error) => response.status(500).send(error));
});

reviewRouter.put('/review/:review_id/edit', (request, response) => {
  const data = request.body;

  if (data && data.review_id.length != 0 && data.review.length != 0)
    reviewService
      .updateReview(data.review_id, data.review, data.game_rating)
      .then(() => response.status(200))
      .catch((error) => response.status(500).send(error));
});

export default reviewRouter;
