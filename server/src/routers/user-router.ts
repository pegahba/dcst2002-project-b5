import express, { request, response } from 'express';
import userService from '../services/user-service';

const userRouter = express.Router();

userRouter.get('/users/:u/:p', (request, response) => {
  const username = String(request.params.u);
  const pass = String(request.params.p);
  userService
    .logIn(username, pass)
    .then((user) => (user ? response.send(user) : response.status(404).send('User not found.')))
    .catch((error) => response.status(500).send(error));
});

userRouter.get('/users/:username', (request, response) => {
  const username = String(request.params.username);
  userService
    .getUserReviews(username)
    .then((rows) => response.send(rows))
    .catch((error) => response.status(404).send(error));
});

userRouter.get('/:username', (request, response) => {
  const username = String(request.params.username);
  userService
    .getUser(username)
    .then((user) => response.send(user))
    .catch((error) => response.status(404).send(error));
});

userRouter.post('/users/add/new', (request, response) => {
  const data = request.body;
  if (data && data.username.length != 0 && data.password.length != 0)
    userService
      .addUser(data.username, data.password)
      .then((rows) => response.send(rows))
      .catch((error) => response.status(500).send(error));
  else response.status(400).send('Missing information');
});

userRouter.put('/users/:username/edit', (request, response) => {
  const data = request.body;
  if (data)
    userService
      .editUser(data.profilePicture, data.bios, data.username)
      .then(() => response.status(200))
      .catch((error) => response.status(500).send(error));
});

export default userRouter;
