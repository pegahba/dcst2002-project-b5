import express from 'express';
import searchService from '../services/search-service';

const searchRouter = express.Router();

searchRouter.get('/search/:search', (request, response) => {
  const title = String(request.params.search);
  searchService
    .getGame('%' + title + '%')
    .then((rows) => (rows ? response.send(rows) : response.status(404).send('Game not found')))
    .catch((error) => response.status(500).send(error));
});

export default searchRouter;
