import express, { request, response } from 'express';
import gameService from '../services/game-service';

const gameRouter = express.Router();

gameRouter.get('/mostReviews', (_request, response) => {
  gameService
    .getMostReviews()
    .then((rows) => response.send(rows))
    .catch((error) => response.status(500).send(error));
});

gameRouter.get('/topRated', (_request, response) => {
  gameService
    .getTopRated()
    .then((rows) => response.send(rows))
    .catch((error) => response.status(500).send(error));
});

gameRouter.get('/lastReviewed', (_request, response) => {
  gameService
    .getLastReviewed()
    .then((rows) => response.send(rows))
    .catch((error) => response.status(500).send(error));
});

gameRouter.get('/games/:id', (request, response) => {
  const id = Number(request.params.id);
  gameService
    .get(id)
    .then((game) => (game ? response.send(game) : response.status(404).send('Game not found')))
    .catch((error) => response.status(500).send(error));
});

gameRouter.get('/all/:page', (request, response) => {
  const page = Number(request.params.page);
  gameService
    .getGamePages(page)
    .then((game) => (game ? response.send(game) : response.status(404).send('Game not found')))
    .catch((error) => response.status(500).send(error));
});

gameRouter.post('/', (request, response) => {
  const data = request.body;
  if (
    data &&
    data.title.length != 0 &&
    data.release_year.length != 0 &&
    data.photo.length != 0 &&
    data.description.length != 0
  )
    gameService
      .addGame(data.title, data.release_year, data.photo, data.description)
      .then((id) => response.send({ id: id }))
      .catch((error) => response.status(500).send(error));
  else response.status(400).send('Missing information about game.');
});

export default gameRouter;
