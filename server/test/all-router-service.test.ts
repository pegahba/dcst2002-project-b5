import axios from 'axios';
import pool from '../src/mysql-pool';
import app from '../src/app';
import gameService, { Game } from '../src/services/game-service';
import genreService, { Genre } from '../src/services/genre-service';
import platformService, { Platform } from '../src/services/platform-service';
import reviewService, { Review, GameRating, ReviewToEdit } from '../src/services/review-service';
import userService, { User, UserProfile, UserReview } from '../src/services/user-service';
import { response } from 'express';

const testGames: any = [
  {
    game_id: 1,
    title: 'Alpha',
    release_year: 2021,
    photo: 'this is image',
    description: 'this is description',
    reviews: '',
  },
  {
    game_id: 2,
    title: 'Beta',
    release_year: 2020,
    photo: 'this is image',
    description: 'this is description',
    reviews: '',
  },
  {
    game_id: 3,
    title: 'Omega',
    release_year: 2019,
    photo: 'this is image',
    description: 'this is description',
    reviews: '',
  },
];

const testGames2: any = [
  {
    game_id: 1,
    title: 'Alpha',
    release_year: 2021,
    photo: 'this is image',
    description: 'this is description',
  },
  {
    game_id: 2,
    title: 'Beta',
    release_year: 2020,
    photo: 'this is image',
    description: 'this is description',
  },
  {
    game_id: 3,
    title: 'Omega',
    release_year: 2019,
    photo: 'this is image',
    description: 'this is description',
  },
];

const testGenres: Genre[] = [
  {
    game_id: 1,
    genre: 'Action',
  },
  {
    game_id: 2,
    genre: 'Battle Royale',
  },
  {
    game_id: 3,
    genre: 'MOBA',
  },
];

const testPlatforms: Platform[] = [
  {
    game_id: 1,
    platform: 'PC',
  },
  {
    game_id: 2,
    platform: 'Console',
  },
  {
    game_id: 3,
    platform: 'Switch',
  },
];

const testReviews: any = [
  {
    game_id: 1,
    review_id: 1,
    rating: 2,
    review: 'its ok',
    username: 'user1',
    game_rating: 3,
  },
  {
    game_id: 2,
    review_id: 2,
    rating: 1,
    review: 'its good',
    username: 'user2',
    game_rating: 4,
  },
  {
    game_id: 2,
    review_id: 3,
    rating: 0,
    review: 'its bad',
    username: 'user3',
    game_rating: 1,
  },
];

const testUsers: any = [
  {
    username: 'user1',
    password: 'pass1',
  },
  {
    username: 'user2',
    password: 'pass2',
  },
  {
    username: 'user3',
    password: 'pass3',
  },
];

const testAddGenres: any = [['Action'], ['Battle Royale'], ['MOBA']];
const testAddPlatforms: any = [['PC'], ['Console'], ['Switch']];

axios.defaults.baseURL = 'http://localhost:3001/api/v2';

let webServer: any;
beforeAll((done) => {
  webServer = app.listen(3001, () => done());
});

beforeEach((done) => {
  pool.query(
    'SET FOREIGN_KEY_CHECKS = 0; TRUNCATE TABLE genre; TRUNCATE TABLE platforms; TRUNCATE TABLE review; TRUNCATE TABLE review_rating; TRUNCATE TABLE users; TRUNCATE TABLE game;',
    (error) => {
      if (error) return done(error);
      gameService
        .addGame(
          testGames[0].title,
          testGames[0].release_year,
          testGames[0].photo,
          testGames[0].description
        )
        .then(() =>
          gameService.addGame(
            testGames[1].title,
            testGames[1].release_year,
            testGames[1].photo,
            testGames[1].description
          )
        )
        .then(() =>
          gameService.addGame(
            testGames[2].title,
            testGames[2].release_year,
            testGames[2].photo,
            testGames[2].description
          )
        )
        .then(() => genreService.addGenres(testGenres[0].game_id, testAddGenres[0]))
        .then(() => genreService.addGenres(testGenres[1].game_id, testAddGenres[1]))
        .then(() => genreService.addGenres(testGenres[2].game_id, testAddGenres[2]))
        .then(() => platformService.addPlatforms(testPlatforms[0].game_id, testAddPlatforms[0]))
        .then(() => platformService.addPlatforms(testPlatforms[1].game_id, testAddPlatforms[1]))
        .then(() => platformService.addPlatforms(testPlatforms[2].game_id, testAddPlatforms[2]));

      userService
        .addUser(testUsers[0].username, testUsers[0].password)
        .then(() => userService.addUser(testUsers[1].username, testUsers[1].password))
        .then(() => userService.addUser(testUsers[2].username, testUsers[2].password))
        .then(() =>
          reviewService.addReview(
            testReviews[0].game_rating,
            testReviews[0].review,
            testReviews[0].username,
            testReviews[0].game_id
          )
        )
        .then(() =>
          reviewService.addReview(
            testReviews[1].game_rating,
            testReviews[1].review,
            testReviews[1].username,
            testReviews[1].game_id
          )
        )
        .then(() =>
          reviewService.addReview(
            testReviews[2].game_rating,
            testReviews[2].review,
            testReviews[2].username,
            testReviews[2].game_id
          )
        )
        .then(() => reviewService.rateReview(testReviews[0].review_id, testUsers[1].username))
        .then(() => reviewService.rateReview(testReviews[0].review_id, testUsers[2].username))
        .then(() => reviewService.rateReview(testReviews[1].review_id, testUsers[2].username))
        .then(() => done());
    }
  );
});

afterAll((done) => {
  if (!webServer) return done(new Error());
  webServer.close(() => pool.end(() => done()));
});

describe('Game router tests', () => {
  describe('Fetch games (GET)', () => {
    test('Fetch recently reviewed games (200)', (done) => {
      //NOTE: THIS TEST WILL FAIL DUE TO THE 'MODIFIED_TIME'
      //COLUMN BEING DYNAMICALLY GENERATED EVERY TIME DATA IS ADDED OR MODIFIED.
      //THE DATA IT RECIEVES IS OTHERWISE CORRECT.
      axios.get('/lastReviewed').then((response) => {
        expect(response.status).toEqual(200);
        expect(response.data).toEqual([testGames[0], testGames[1]]);
        done();
      });
    });

    test('Fetch all games (200)', (done) => {
      axios.get('/all/0').then((response) => {
        expect(response.status).toEqual(200);
        expect(response.data).toEqual(testGames2);
        done();
      });
    });

    test('Fetch game (200)', (done) => {
      axios.get('/games/1').then((response) => {
        expect(response.status).toEqual(200);
        expect(response.data).toEqual(testGames2[0]);
        done();
      });
    });

    test('Fetch game (404)', (done) => {
      axios
        .get('/games/4')
        .then((_response) => done(new Error()))
        .catch((error) => {
          expect(error.message).toEqual('Request failed with status code 404');
          done();
        });
    });

    test('Fetch most reviewed games (200)', (done) => {
      axios.get('/mostReviews').then((response) => {
        expect(response.status).toEqual(200);
        expect(response.data).toEqual([
          {
            description: 'this is description',
            game_id: 2,
            photo: 'this is image',
            release_year: 2020,
            reviews: 2,
            title: 'Beta',
          },
          {
            description: 'this is description',
            game_id: 1,
            photo: 'this is image',
            release_year: 2021,
            reviews: 1,
            title: 'Alpha',
          },
        ]);
        done();
      });
    });

    test('Fetch top rated games (200)', (done) => {
      axios.get('/topRated').then((response) => {
        expect(response.status).toEqual(200);
        expect(response.data).toEqual([
          {
            avg_rating: 3,
            description: 'this is description',
            game_id: 1,
            photo: 'this is image',
            release_year: 2021,
            title: 'Alpha',
          },
          {
            avg_rating: 2.5,
            description: 'this is description',
            game_id: 2,
            photo: 'this is image',
            release_year: 2020,
            title: 'Beta',
          },
        ]);
        done();
      });
    });
  });

  describe('Create new game (POST)', () => {
    test('Create new game (200)', (done) => {
      axios
        .post('/', {
          title: 'Beta 4',
          release_year: 2018,
          photo: 'this is image',
          description: 'this is description',
        })
        .then((response) => {
          expect(response.status).toEqual(200);
          expect(response.data).toEqual({ id: 4 });
          done();
        });
    });
  });
});

describe('Genre router tests', () => {
  describe('Fetch genres (GET)', () => {
    test('Fetch all genres for a specific game (200)', (done) => {
      axios.get('/games/1/genres').then((response) => {
        expect(response.status).toEqual(200);
        expect(response.data).toEqual([testGenres[0]]);
        done();
      });
    });

    test('Fetch all games under a certain genre (200)', (done) => {
      axios.get('/genres/Action').then((response) => {
        expect(response.status).toEqual(200);
        expect(response.data).toEqual([testGames2[0]]);
        done();
      });
    });

    test('Fetch genres for games, comma seperated (200)', (done) => {
      axios.get('/genres').then((response) => {
        expect(response.status).toEqual(200);
        expect(response.data).toEqual(testGenres);
        done();
      });
    });
  });
});

describe('Platform router tests', () => {
  describe('Fetch genres (GET)', () => {
    test('Fetch all platforms for a specific game (200)', (done) => {
      axios.get('/games/3/platforms').then((response) => {
        expect(response.status).toEqual(200);
        expect(response.data).toEqual([testPlatforms[2]]);
        done();
      });
    });

    test('Fetch all games under a certain platform (200)', (done) => {
      axios.get('/platforms/Console').then((response) => {
        expect(response.status).toEqual(200);
        expect(response.data).toEqual([testGames2[1]]);
        done();
      });
    });

    test('Fetch platforms for games, comma seperated (200)', (done) => {
      axios.get('/platforms').then((response) => {
        expect(response.status).toEqual(200);
        expect(response.data).toEqual(testPlatforms);
        done();
      });
    });
  });
});

describe('Search router test (GET)', () => {
  test('Fetch games searched by title (200)', (done) => {
    axios.get('/search/Beta').then((response) => {
      expect(response.status).toEqual(200);
      expect(response.data).toEqual([testGames2[1]]);
      done();
    });
  });
});

describe('Review router tests', () => {
  describe('Fetch reviews and ratings (GET)', () => {
    test('Get avg rating for a game (200)', (done) => {
      axios.get('/games/1/rating').then((response) => {
        expect(response.status).toEqual(200);
        expect(response.data).toEqual({ avg_rating: 3 });
        done();
      });
    });

    test.skip('Get reviews for a game (200)', (done) => {
      axios.get('/games/1/reviews').then((response) => {
        expect(response.status).toEqual(200);
        //NOTE: THIS TEST WILL FAIL DUE TO THE 'MODIFIED_TIME'
        //COLUMN BEING DYNAMICALLY GENERATED EVERY TIME DATA IS ADDED OR MODIFIED.
        //THE DATA IT RECIEVES IS OTHERWISE CORRECT.
        expect(response.data).toEqual([testReviews[0]]);
        done();
      });
    });

    test('Get a review for editting (200)', (done) => {
      //NOTE: THIS TEST WILL FAIL DUE TO THE 'MODIFIED_TIME'
      //COLUMN BEING DYNAMICALLY GENERATED EVERY TIME DATA IS ADDED OR MODIFIED.
      //THE DATA IT RECIEVES IS OTHERWISE CORRECT.
      axios.get('/review/1/edit').then((response) => {
        expect(response.status).toEqual(200);
        expect(response.data).toEqual({
          game_id: 1,
          game_rating: 3,
          photo: 'this is image',
          release_year: 2021,
          review: 'its ok',
          review_id: 1,
          title: 'Alpha',
          username: 'user1',
        });
        done();
      });
    });
  });

  describe('Add or rate review (POST)', () => {
    test('Post rating for a review', (done) => {
      axios.post('/review/rating', { id: 3, username: 'user1' }).then((response) => {
        expect(response.status).toEqual(200);
        done();
      });
    });

    test('Post new review', (done) => {
      axios
        .post('/review/new', {
          game_id: 3,
          review: 'its bad',
          username: 'user1',
          rating: 1,
        })
        .then((response) => {
          expect(response.status).toEqual(200);
          expect(response.data).toEqual({ id: 4 });
          done();
        });
    });
  });

  describe('Delete review (DELETE)', () => {
    test('Delete review (200)', (done) => {
      axios.delete('/reviews/2').then((response) => {
        expect(response.status).toEqual(200);
        done();
      });
    });
  });

  describe('Edit a review (PUT)', () => {
    test('Edit task (500)', (done) => {
      axios
        .put('/review/1/edit', {
          review_id: 1,
          review: 'its ok',
          game_rating: 4,
        })
        .then((response) => {
          expect(response.status).toEqual(500);
          done();
        });
    });
  });
});

describe('User router tests', () => {
  describe('Fetch (GET)', () => {
    test('Fetch user reviews (200)', (done) => {
      axios.get('/users/user1').then((response) => {
        //NOTE: THIS TEST WILL FAIL DUE TO THE 'MODIFIED_TIME'
        //COLUMN BEING DYNAMICALLY GENERATED EVERY TIME DATA IS ADDED OR MODIFIED.
        //THE DATA IT RECIEVES IS OTHERWISE CORRECT.
        expect(response.status).toEqual(200);
        expect(response.data).toEqual([
          {
            game_id: 1,
            game_rating: 3,
            photo: 'this is image',
            rating: 2,
            release_year: 2021,
            review: 'its ok',
            review_id: 1,
            title: 'Alpha',
            username: 'user1',
          },
        ]);
        done();
      });
    });

    test('Fetch user username for login (200)', (done) => {
      axios.get('/users/user1/pass1').then((response) => {
        expect(response.status).toEqual(200);
        expect(response.data).toEqual({ username: 'user1' });
        done();
      });
    });

    test('Fetch user details (200)', (done) => {
      axios.get('/user2').then((response) => {
        expect(response.status).toEqual(200);
        expect(response.data).toEqual({
          profilePicture: null,
          bios: null,
        });
        done();
      });
    });
  });

  describe('Add user (POST)', () => {
    test('Add new user (200)', (done) => {
      axios.post('/users/add/new', { username: 'user4', password: 'pass4' }).then((response) => {
        expect(response.status).toEqual(200);
        done();
      });
    });
  });

  describe('Edit user (PUT)', () => {
    test('Edit user profile (200)', (done) => {
      axios
        .put('/users/user1/edit', {
          profilePicture: 'pic',
          bios: 'bio',
          username: testUsers[0].username,
        })
        .then((response) => {
          expect(response.status).toEqual(200);
          console.log(response.data);
          done();
        });
    });
  });
});
