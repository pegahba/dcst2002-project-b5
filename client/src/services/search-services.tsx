import { assertTSAnyKeyword } from '@babel/types';
import axios from 'axios';
import { Game } from './game-services';

axios.defaults.baseURL = 'http://localhost:3000/api/v2';

class SearchService {
  getGame(title: string) {
    return axios
    .get<Game[]>('/search/' + title).then((response) => response.data);
  }
}

const searchService = new SearchService();
export default searchService;
