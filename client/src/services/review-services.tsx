import { assertTSAnyKeyword } from '@babel/types';
import axios from 'axios';
import { SupportInfo } from 'prettier';
import { UserReview } from './user-services';

axios.defaults.baseURL = 'http://localhost:3000/api/v2';

export type Review = {
  review_id: number;
  rating: number;
  review: string;
  username: string;
  game_rating: number;
  modified_time: string;
};

export type GameRating = {
  avg_rating: number;
};

export type ReviewToEdit = {
  review_id: number;
  review: string;
  game_rating: number;
  title: string;
  release_year: number;
  photo: string;
  modified_time: string;
};

class ReviewService {
  getReviews(id: number) {
    return axios.get<Review[]>('/games/' + id + '/reviews').then((response) => response.data);
  }

  getReview(review_id: number) {
    return axios
      .get<ReviewToEdit>('/review/' + review_id + '/edit')
      .then((response) => response.data);
  }

  getRating(id: number) {
    return axios.get<GameRating>('/games/' + id + '/rating').then((response) => response.data);
  }

  rateReview(id: number, username: string) {
    return axios
      .post<{ id: number }>('/review/rating', { id: id, username: username })
      .then((response) => response.data);
  }

  addReview(rating: number, review: string, username: string, game_id: number) {
    return axios
      .post<{ id: number }>('/review/new', {
        rating: rating,
        review: review,
        username: username,
        game_id: game_id,
      })
      .then((response) => response.data.id);
  }

  deleteReview(id: number) {
    return axios.delete<UserReview>('/reviews/' + id).then((response) => response.data);
  }

  editReview(review_id: number, review: string, game_rating: number) {
    return axios
      .put<{ id: number }>('/review/' + review_id + '/edit', {
        review_id: review_id,
        review: review,
        game_rating: game_rating,
      })
      .then((response) => response.data.id);
  }
}

const reviewService = new ReviewService();
export default reviewService;
