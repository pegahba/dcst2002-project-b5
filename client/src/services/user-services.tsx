import { assertTSAnyKeyword } from '@babel/types';
import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3000/api/v2';

export type User = {
  username: string;
};

export type UserReview = {
  review_id: number;
  review: string;
  username: string;
  game_rating: number;
  game_id: number;
  title: string;
  release_year: number;
  photo: string;
  rating: number;
  modified_time: string;
};

export type UserProfile = {
  profilePicture: string;
  bios: string;
};

class UserService {
  logIn(username: string, password: string) {
    return axios.get<User>('/users/' + username + '/' + password).then((response) => response.data);
  }

  getUserReviews(username: string) {
    return axios.get<UserReview[]>('/users/' + username).then((response) => response.data);
  }

  addUser(username: string, password: string) {
    return axios
      .post<User>('/users/add/new', {
        username: username,
        password: password,
      })
      .then((response) => response.data);
  }

  getUser(username: string) {
    return axios.get<UserProfile>('/' + username).then((response) => response.data);
  }

  editUser(profilePicture: string, bios: string, username: string) {
    return axios
      .put<UserProfile>('/users/' + username + '/edit', {
        profilePicture: profilePicture,
        bios: bios,
        username: username,
      })
      .then((response) => response.data);
  }
}

const userService = new UserService();
export default userService;
