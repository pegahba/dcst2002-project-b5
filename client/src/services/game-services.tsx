import { assertTSAnyKeyword } from '@babel/types';
import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3000/api/v2';

export type Game = {
  game_id: number;
  title: string;
  release_year: number;
  photo: string;
  description: string;
};

class GameService {
  get(id: number) {
    return axios.get<Game>('/games/' + id).then((response) => response.data);
  }

  getMostReviews() {
    return axios.get<Game[]>('/mostReviews').then((response) => response.data);
  }

  getTopRated() {
    return axios.get<Game[]>('/topRated').then((response) => response.data);
  }

  getGamePages(page: number) {
    return axios.get<Game[]>('/all/' + page).then((response) => response.data);
  }

  getLastReviewed() {
    return axios.get<Game[]>('/lastReviewed').then((response) => response.data);
  }

  addGame(title: string, release_year: number, photo: string, description: string) {
    return axios
      .post<{ id: number }>('/', {
        title: title,
        release_year: release_year,
        photo: photo,
        description: description,
      })
      .then((response) => response.data.id);
  }
}

const gameService = new GameService();
export default gameService;
