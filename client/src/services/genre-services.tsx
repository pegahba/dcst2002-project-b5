import { assertTSAnyKeyword } from '@babel/types';
import axios from 'axios';
import { Game } from './game-services';

axios.defaults.baseURL = 'http://localhost:3000/api/v2';

export type Genre = {
  game_id: number;
  genre: string;
};

class GenreService {
  getGenres(id: number) {
    return axios.get<Genre[]>('/games/' + id + '/genres').then((response) => response.data);
  }

  getGenreList(genre: string) {
    return axios.get<Game[]>('/genres/' + genre).then((response) => response.data);
  }

  getAllGenres() {
    return axios.get<Genre[]>('/genres').then((response) => response.data);
  }

  addGenres(id: number, genres: []) {
    return axios
      .post<{ id: number }>('/new/genres', {
        game_id: id,
        genres: genres,
      })
      .then((response) => response.data);
  }
}

const genreService = new GenreService();
export default genreService;
