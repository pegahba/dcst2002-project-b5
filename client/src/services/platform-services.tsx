import { assertTSAnyKeyword } from '@babel/types';
import axios from 'axios';
import { Game } from './game-services';

axios.defaults.baseURL = 'http://localhost:3000/api/v2';

export type Platform = {
  game_id: number;
  platform: string;
};

class PlatformService {
  getPlatforms(id: number) {
    return axios.get<Platform[]>('/games/' + id + '/platforms').then((response) => response.data);
  }

  getPlatformList(platform: string) {
    return axios.get<Game[]>('/platforms/' + platform).then((response) => response.data);
  }

  getAllPlatforms() {
    return axios.get<Platform[]>('/platforms').then((response) => response.data);
  }

  addPlatforms(id: number, platforms: []) {
    return axios
      .post<{ id: number }>('/new/platforms', {
        game_id: id,
        platforms: platforms,
      })
      .then((response) => response.data);
  }
}

const platformService = new PlatformService();
export default platformService;
