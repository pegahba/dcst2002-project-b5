import * as React from 'react';
import { Component } from 'react-simplified';
import { Alert, Card, Row, Column, Form, Button } from '../widgets';
import { NavLink } from 'react-router-dom';
import gameService, { Game } from '../services/game-services';
import genreService, { Genre } from '../services/genre-services';
import platformService, { Platform } from '../services/platform-services';
import reviewService, { Review, GameRating } from '../services/review-services';
import { User } from '../services/user-services';
import { createHashHistory } from 'history';

const history = createHashHistory();

export class GameList extends Component<{ match: { params: { page: number } } }> {
  games: Game[] = [];
  page: number = Number(this.props.match.params.page);
  x: number = 1;
  return: number = 0;

  render() {
    return (
      <>
        <h5 id="pageTitle">Page {this.x}</h5>
        <div id="left-right-btn">
          <div id="left-btn">
            <Button.Light
              onClick={() => {
                this.page <= 0
                  ? Alert.info('You are at the start')
                  : ((this.page = Number(this.page - 30)),
                    history.push('/all/' + this.page),
                    this.x--);
              }}
            >
              Previous Page
            </Button.Light>
          </div>
          <p />
          <div id="right-btn">
            <Button.Light
              onClick={() => {
                this.games.length !== 30
                  ? Alert.info('You have reached the end')
                  : ((this.page = Number(this.page + 30)),
                    history.push('/all/' + this.page),
                    this.x++);
              }}
            >
              Next Page
            </Button.Light>
          </div>
        </div>
        <div id="gamePage">
          {this.games.map((game) => (
            <div id="flex-items">
              <NavLink to={'/games/' + game.game_id}>
                <img width="100%" src={game.photo}></img>
              </NavLink>
            </div>
          ))}
        </div>{' '}
      </>
    );
  }
  mounted() {
    if (this.page < 0) {
      this.page = 0;
      this.x = this.page / 30 + 1;
      history.push('/all/' + 0);

      gameService
        .getGamePages(this.props.match.params.page)
        .then((games) => (this.games = games))
        .catch((error) => Alert.danger('Something went wrong; ' + error.message));
    } else if (this.page != this.props.match.params.page) {
      this.page = Number(this.props.match.params.page);
      this.x = this.page / 30 + 1;

      gameService
        .getGamePages(this.props.match.params.page)
        .then((games) => (this.games = games))
        .catch((error) => Alert.danger('Something went wrong; ' + error.message));
    } else {
      this.x = this.page / 30 + 1;
      gameService
        .getGamePages(this.props.match.params.page)
        .then((games) => (this.games = games))
        .catch((error) => Alert.danger('Something went wrong; ' + error.message));
    }
  }
}

export class GamePage extends Component<{ match: { params: { id: number } } }> {
  user: User = JSON.parse(sessionStorage.getItem('user') || '[]');
  game: Game = {
    game_id: 0,
    title: '',
    release_year: 0,
    photo: '',
    description: '',
  };
  rating: GameRating = { avg_rating: 0 };
  genres: Genre[] = [];
  platforms: Platform[] = [];
  reviews: Review[] = [];
  user_review: string = '';
  user_rating: number = 0;
  possible_rating = [1, 2, 3, 4, 5];
  state = { count: 0 };

  render() {
    return (
      <>
        <div id="oneGamePage">
          <Card title={this.game.title + ' (' + this.game.release_year + ')'}>
            <Row>
              <Column width={10}>
                {this.genres.map((g) => (
                  <>
                    <Button.Success
                      key={g.genre}
                      onClick={() => {
                        history.push('/genres/' + g.genre);
                      }}
                    >
                      {g.genre}
                    </Button.Success>{' '}
                  </>
                ))}
                <p />
                <Card
                  title={
                    'Average Rating: ' +
                    (this.rating.avg_rating == null ? '-' : this.rating.avg_rating + '/5')
                  }
                >
                  {this.game.description}
                  <p />
                  Available On:{' '}
                  {this.platforms.map((p) => (
                    <>
                      <Button.Success
                        key={p.platform}
                        onClick={() => {
                          history.push('/platforms/' + p.platform);
                        }}
                      >
                        {p.platform}
                      </Button.Success>{' '}
                    </>
                  ))}
                </Card>
                <p />
              </Column>
              <Column width={2} right>
                <img width="100%" src={this.game.photo}></img>
              </Column>
            </Row>
          </Card>
          <Card title="User reviews for this game">
            <Row>
              <Column width={8}>
                {this.reviews.length != 0 ? (
                  this.reviews.map((r) => (
                    <Card
                      title={
                        <>
                          <Row>
                            <Column>
                              <NavLink to={'/users/' + r.username}>{r.username}</NavLink>
                            </Column>
                            <Column right>
                              <small>
                                <small>{r.modified_time.substr(0, 10)}</small>
                              </small>
                            </Column>
                          </Row>
                        </>
                      }
                      key={r.review_id}
                    >
                      <Row>
                        <Column width={2}>
                          {r.game_rating != null ? '  ' + r.game_rating + '/5' : ''}
                        </Column>
                        <Column width={6}>{r.review}</Column>
                        <Column right>
                          <Button.Danger
                            onClick={() => {
                              this.user.username === undefined || ''
                                ? Alert.warning('You need to be logged in to do that!')
                                : (this.setState({ count: this.state.count + 1 }, () =>
                                    this.mounted()
                                  ),
                                  reviewService
                                    .rateReview(r.review_id, this.user.username)
                                    .then()
                                    .catch((error) =>
                                      Alert.info('You have already liked this review!')
                                    ));
                            }}
                          >
                            ♡
                          </Button.Danger>
                        </Column>
                        <Column>
                          <small>
                            {r.rating != null
                              ? r.rating != 1
                                ? 'Liked by ' + r.rating + ' others'
                                : 'Liked by ' + r.rating + ' other'
                              : 'Be the first to like this!'}
                          </small>
                        </Column>
                      </Row>
                    </Card>
                  ))
                ) : (
                  <Card title="No reviews yet."></Card>
                )}
              </Column>
              <Column>
                <Card title="Have you played this game? Tell others what you liked or disliked about it!">
                  {this.user.username === undefined || '' ? (
                    <Row>
                      <Button.Info onClick={() => history.push('/login')}>
                        Log in to review this game
                      </Button.Info>
                    </Row>
                  ) : (
                    <>
                      <Row>
                        <Column>Rating:</Column>
                        <Column>
                          <select
                            onChange={(event) => {
                              this.user_rating = Number(event.currentTarget.value);
                            }}
                          >
                            <option value="">-</option>
                            {this.possible_rating.map((rating) => {
                              return <option value={rating}>{rating}</option>;
                            })}
                          </select>
                        </Column>
                        <p />
                        <Column>
                          <Form.Label>Review:</Form.Label>
                        </Column>
                        <Column>
                          <Form.Textarea
                            value={this.user_review}
                            onChange={(event) => {
                              this.user_review = event.currentTarget.value;
                            }}
                            rows={5}
                          />
                        </Column>
                        <p />
                        <Button.Success
                          onClick={() => {
                            reviewService
                              .addReview(
                                this.user_rating,
                                this.user_review,
                                this.user.username,
                                this.game.game_id
                              )
                              .catch((error) =>
                                Alert.warning('You have already reviewed this game')
                              );
                            this.setState({ count: this.state.count + 1 }, () => this.mounted());
                            this.user_review = '';
                          }}
                        >
                          Submit Review
                        </Button.Success>
                      </Row>
                    </>
                  )}
                </Card>
              </Column>
            </Row>
          </Card>
        </div>
      </>
    );
  }

  mounted() {
    gameService
      .get(this.props.match.params.id)
      .then((game) => (this.game = game))
      .catch((error) => Alert.danger('Error getting game: ' + error.message));

    genreService
      .getGenres(this.props.match.params.id)
      .then((genres) => (this.genres = genres))
      .catch((error) => Alert.danger('Something went wrong; ' + error.message));

    platformService
      .getPlatforms(this.props.match.params.id)
      .then((platforms) => (this.platforms = platforms))
      .catch((error) => Alert.danger('Something went wrong; ' + error.message));

    reviewService
      .getReviews(this.props.match.params.id)
      .then((reviews) => (this.reviews = reviews))
      .catch((error) => Alert.danger('Something went wrong; ' + error.message));

    reviewService
      .getRating(this.props.match.params.id)
      .then((rating) => (this.rating = rating))
      .catch((error) => Alert.danger('Something went wrong; ' + error.message));
  }
}

export class GameNew extends Component {
  title: string = '';
  release_year: number = 2021;
  photo: string = '';
  description: string = '';
  genre1: string = '';
  genre2: string = '';
  genre3: string = '';
  platform1: string = '';
  platform2: string = '';
  platform3: string = '';

  platforms: any = [];
  genres: any = [];

  render() {
    return (
      <>
        <div id="oneGamePage">
          <Card title="Add A New Game">
            <Row>
              <Column width={10}>
                <Form.Input
                  type="text"
                  placeholder="Title"
                  value={this.title}
                  onChange={(e) => (this.title = e.currentTarget.value)}
                />
                <p />
                <Form.Input
                  type="number"
                  placeholder="Release Year"
                  value={this.release_year}
                  onChange={(e) => (this.release_year = Number(e.currentTarget.value))}
                />
                <p />
                <Row>
                  <Column>
                    <Form.Input
                      type="text"
                      placeholder="Genre"
                      value={this.genre1}
                      onChange={(e) => (this.genre1 = e.currentTarget.value)}
                    />
                  </Column>
                  <Column>
                    <Form.Input
                      type="text"
                      placeholder="Genre"
                      value={this.genre2}
                      onChange={(e) => (this.genre2 = e.currentTarget.value)}
                    />
                  </Column>
                  <Column>
                    <Form.Input
                      type="text"
                      placeholder="Genre"
                      value={this.genre3}
                      onChange={(e) => (this.genre3 = e.currentTarget.value)}
                    />
                  </Column>
                </Row>
                <p />
                <Row>
                  <Column>
                    <Form.Input
                      type="text"
                      placeholder="Platform"
                      value={this.platform1}
                      onChange={(e) => (this.platform1 = e.currentTarget.value)}
                    />
                  </Column>
                  <Column>
                    <Form.Input
                      type="text"
                      placeholder="Platform"
                      value={this.platform2}
                      onChange={(e) => (this.platform2 = e.currentTarget.value)}
                    />
                  </Column>
                  <Column>
                    <Form.Input
                      type="text"
                      placeholder="Platform"
                      value={this.platform3}
                      onChange={(e) => (this.platform3 = e.currentTarget.value)}
                    />
                  </Column>
                </Row>
                <p />
                <Form.Textarea
                  type="text"
                  placeholder="Description (max 500 characters)"
                  value={this.description}
                  maxlength={500}
                  onChange={(e) => (this.description = e.currentTarget.value)}
                />
                <p />
                <Form.Input
                  type="text"
                  placeholder="Cover Photo (link)"
                  value={this.photo}
                  onChange={(e) => (this.photo = e.currentTarget.value)}
                />
              </Column>
              <Column width={2}>
                {this.photo.length != 0 ? (
                  <img width="90%" src={this.photo}></img>
                ) : (
                  <img width="90%" src="https://i.imgur.com/HfDzmtW.png"></img>
                )}
              </Column>
            </Row>
            <p />
            <Row>
              <Button.Success
                onClick={() => {
                  this.genres.push(this.genre1, this.genre2, this.genre3),
                    this.platforms.push(this.platform1, this.platform2, this.platform3),
                    this.addNewGame();
                }}
              >
                Add Game
              </Button.Success>
            </Row>
          </Card>
        </div>
      </>
    );
  }

  addNewGame() {
    gameService
      .addGame(this.title, this.release_year, this.photo, this.description)
      .then(
        (id) => (
          genreService
            .addGenres(id, this.genres)
            .then()
            .catch((error) => Alert.danger('Something went wrong; ' + error.message)),
          platformService
            .addPlatforms(id, this.platforms)
            .then()
            .catch((error) => Alert.danger('Something went wrong; ' + error.message)),
          history.push('/games/' + id)
        )
      )
      .catch((error) => Alert.danger('Error adding game; ' + error));
  }
}

export class GameTop extends Component {
  mostReviews: Game[] = [];
  topRated: Game[] = [];
  lastReviews: Game[] = [];

  render() {
    return (
      <>
        <h3 id="pageTitle">Recently Reviewed</h3>
        <div id="gamePage">
          {this.lastReviews.map((game) => (
            <div id="flex-items">
              <NavLink to={'/games/' + game.game_id}>
                <img width="100%" src={game.photo}></img>
              </NavLink>
            </div>
          ))}
        </div>
        <p />
        <h3 id="pageTitle">Most Reviewed Games</h3>
        <div id="gamePage">
          {this.mostReviews.map((game) => (
            <div id="flex-items">
              <NavLink to={'/games/' + game.game_id}>
                <img width="100%" src={game.photo}></img>
              </NavLink>
            </div>
          ))}
        </div>
        <p />
        <h3 id="pageTitle">Highest Rated Games</h3>
        <div id="gamePage">
          {this.topRated.map((game) => (
            <div id="flex-items">
              <NavLink to={'/games/' + game.game_id}>
                <img width="100%" src={game.photo}></img>
              </NavLink>
            </div>
          ))}
        </div>
      </>
    );
  }

  mounted() {
    gameService
      .getMostReviews()
      .then((games) => (this.mostReviews = games))
      .catch((error) => Alert.danger('Something went wrong; ' + error.message));

    gameService
      .getTopRated()
      .then((games) => (this.topRated = games))
      .catch((error) => Alert.danger('Something went wrong; ' + error.message));

    gameService
      .getLastReviewed()
      .then((games) => (this.lastReviews = games))
      .catch((error) => Alert.danger('Something went wrong; ' + error.message));
  }
}
