import * as React from 'react';
import { Component } from 'react-simplified';
import { Alert, Card, Row, Column, Form, Button } from '../widgets';
import userService, { User, UserReview, UserProfile } from '../services/user-services';
import reviewService, { ReviewToEdit } from '../services/review-services';
import { createHashHistory } from 'history';
import { NavLink } from 'react-router-dom';
import { SHA256 } from 'crypto-js';

const history = createHashHistory();

export class UserLogIn extends Component {
  user: User = JSON.parse(sessionStorage.getItem('user') || '[]');

  username: string = '';
  password: string = '';
  userInfo: any = [];

  render() {
    if (this.user.username !== undefined || '') {
      history.push('/users/' + this.user.username);
      return <></>;
    } else {
      return (
        <>
          <div id="oneGamePage">
            <div id="userLogin">
              <Card title="Log In">
                <Row>
                  <Form.Input
                    type="text"
                    placeholder="Username"
                    value={this.username}
                    onChange={(e) => (this.username = e.currentTarget.value)}
                  />
                  <p />
                  <Form.Input
                    type="password"
                    placeholder="Password"
                    value={this.password}
                    onChange={(e) => (this.password = e.currentTarget.value)}
                  />
                  <p />
                  <Button.Success
                    onClick={() => {
                      userService
                        .logIn(this.username, JSON.stringify(SHA256(this.password)))
                        .then(
                          (user) => (
                            (this.userInfo = user),
                            sessionStorage.setItem('user', JSON.stringify(this.userInfo)),
                            history.push('/users/' + user.username)
                          )
                        )
                        .catch((error) => Alert.danger('Wrong username or password.'));
                    }}
                  >
                    Log In
                  </Button.Success>
                  <p />
                  <Button.Secondary
                    onClick={() => {
                      history.push('/users/add/new');
                    }}
                  >
                    Don't have an account? Sign up!
                  </Button.Secondary>
                </Row>
              </Card>
            </div>
          </div>
        </>
      );
    }
  }
}

export class UserPage extends Component<{ match: { params: { username: string } } }> {
  userProfile: UserProfile = { profilePicture: '', bios: '' };
  user: User = JSON.parse(sessionStorage.getItem('user') || '[]');
  userReviews: UserReview[] = [];
  state = { count: 0 };

  render() {
    return (
      <>
        <Card title={this.props.match.params.username} key="details">
          <Row>
            <Column width={1}>
              <img width="100%" src={this.userProfile.profilePicture}></img>
            </Column>
            <Column>{this.userProfile.bios}</Column>
            <Column right>
              {this.user.username === this.props.match.params.username ? (
                <>
                  <Button.Light
                    onClick={() => {
                      sessionStorage.removeItem('user');
                      history.push('/');
                    }}
                  >
                    Log Out
                  </Button.Light>
                  <p />
                  <Button.Info
                    onClick={() => {
                      history.push('/users/' + this.user.username + '/edit');
                    }}
                  >
                    Edit profile
                  </Button.Info>
                </>
              ) : (
                ''
              )}
            </Column>
          </Row>
        </Card>
        <div id="oneGamePage">
          <Card title="Reviews">
            {this.userReviews.length != 0
              ? this.userReviews.map((review) => (
                  <Card
                    title={
                      <NavLink to={'/games/' + review.game_id}>
                        {review.title + ' (' + review.release_year + ') '}
                      </NavLink>
                    }
                  >
                    <Row>
                      <Column width={1}>
                        <img width="75%" src={review.photo}></img>
                      </Column>

                      <Column width={5}>
                        <small>Reviewed on: {review.modified_time.substr(0, 10)}</small>

                        <p />
                        {review.game_rating != null ? 'Rating: ' + review.game_rating + '/5' : ''}
                        <p />
                        {review.review}
                      </Column>

                      <Column width={3}>
                        <p />
                        <Button.Danger
                          onClick={() => {
                            this.user.username === undefined || ''
                              ? Alert.warning('You need to be logged in to do that!')
                              : (this.setState({ count: this.state.count + 1 }, () =>
                                  this.mounted()
                                ),
                                reviewService
                                  .rateReview(review.review_id, this.user.username)
                                  .then()
                                  .catch((error) =>
                                    Alert.warning('You have already liked this review!')
                                  ));
                          }}
                        >
                          ♡
                        </Button.Danger>{' '}
                        <small>
                          {review.rating != null
                            ? review.rating != 1
                              ? 'Liked by ' + review.rating + ' others'
                              : 'Liked by ' + review.rating + ' other'
                            : 'Be the first to like this!'}
                        </small>
                      </Column>
                      <Column right>
                        {this.user.username === this.props.match.params.username ? (
                          <>
                            <Button.Info
                              onClick={() => {
                                history.push('/review/' + review.review_id + '/edit');
                              }}
                            >
                              Edit
                            </Button.Info>
                            <p />
                            <Button.Secondary
                              onClick={() => {
                                this.setState({ count: this.state.count + 1 }, () =>
                                  this.mounted()
                                );
                                reviewService
                                  .deleteReview(review.review_id)
                                  .then(() => {
                                    Alert.success('Review deleted successfully.');
                                  })
                                  .catch((error) =>
                                    Alert.danger('Something went wrong; ' + error.message)
                                  );
                              }}
                            >
                              Delete
                            </Button.Secondary>
                          </>
                        ) : (
                          ''
                        )}
                      </Column>
                    </Row>
                  </Card>
                ))
              : 'No reviews yet.'}
          </Card>
        </div>
      </>
    );
    // }
  }

  mounted() {
    userService
      .getUser(this.props.match.params.username)
      .then((user) => (this.userProfile = user))
      .catch((error) => Alert.danger('Something went wrong; ' + error.message));

    userService
      .getUserReviews(this.props.match.params.username)
      .then((reviews) => (this.userReviews = reviews))
      .catch((error) => Alert.danger('Something went wrong; ' + error.message));
  }
}

export class ReviewEdit extends Component<{ match: { params: { review_id: number } } }> {
  user: User = JSON.parse(sessionStorage.getItem('user') || '[]');
  review: ReviewToEdit = {
    review_id: 0,
    review: '',
    game_rating: 0,
    title: '',
    release_year: 0,
    photo: '',
    modified_time: '',
  };
  possible_rating = [1, 2, 3, 4, 5];

  render() {
    return (
      <>
        <div id="oneGamePage">
          <Card
            title={
              'Edit Your Review for ' + this.review.title + ' (' + this.review.release_year + ')'
            }
          >
            <Row>
              <Column width={2}>
                <img width="85%" src={this.review.photo}></img>
              </Column>
              <Column>
                <Column width={2}>
                  <Form.Label>Rating:</Form.Label>{' '}
                  <select
                    value={this.review.game_rating}
                    onChange={(event) => {
                      this.review.game_rating = Number(event.currentTarget.value);
                    }}
                  >
                    <option value="">-</option>
                    {this.possible_rating.map((rating) => {
                      return <option value={rating}>{rating}</option>;
                    })}
                  </select>
                </Column>

                <Form.Label>Review:</Form.Label>
                <Column>
                  <Form.Textarea
                    value={this.review.review}
                    onChange={(event) => {
                      this.review.review = event.currentTarget.value;
                    }}
                    rows={6}
                  />
                </Column>
              </Column>
            </Row>
            <p />
            <Row>
              <Button.Success
                onClick={() => {
                  reviewService.editReview(
                    this.review.review_id,
                    this.review.review,
                    this.review.game_rating
                  );
                  history.push('/users/' + this.user.username);
                }}
              >
                {' '}
                Save Changes
              </Button.Success>
            </Row>
          </Card>
        </div>
      </>
    );
  }

  mounted() {
    reviewService
      .getReview(this.props.match.params.review_id)
      .then((review) => (this.review = review))
      .catch((error) => Alert.danger('Something went wrong; ' + error.message));
  }
}

export class UserNew extends Component {
  username: string = '';
  password: string = '';
  userInfo: any = [];

  render() {
    return (
      <>
        <div id="oneGamePage">
          <div id="userLogin">
            <Card title="Create New User">
              <Row>
                <Form.Label>Username: </Form.Label>
                <Form.Input
                  type="text"
                  placeholder="username"
                  value={this.username}
                  onChange={(e) => (this.username = e.currentTarget.value)}
                />
                <p />
                <Form.Label>Password:</Form.Label>
                <Form.Input
                  type="password"
                  placeholder="password"
                  value={this.password}
                  onChange={(e) => (this.password = e.currentTarget.value)}
                />
                <p />
                <Button.Success
                  onClick={() => {
                    userService
                      .addUser(this.username, JSON.stringify(SHA256(this.password)))
                      .then(() => history.push('/login'))
                      .catch((error) => Alert.danger('Something went wrong; ' + error.message));
                  }}
                >
                  Register
                </Button.Success>
              </Row>
            </Card>
          </div>
        </div>
      </>
    );
  }
}

export class UserEdit extends Component<{ match: { params: { username: string } } }> {
  user: User = JSON.parse(sessionStorage.getItem('user') || '[]');
  userProfile: UserProfile = { profilePicture: '', bios: '' };

  render() {
    return (
      <>
        <div id="oneGamePage">
          <Card title="Edit your profile">
            <Row>
              <Column width={2}>
                <img width="100%" src={this.userProfile.profilePicture}></img>
              </Column>
              <Column>
                <Form.Label>Profile picture:</Form.Label>
                <Form.Input
                  type="text"
                  placeholder="Link to image"
                  value={this.userProfile.profilePicture}
                  onChange={(e) => (this.userProfile.profilePicture = e.currentTarget.value)}
                ></Form.Input>

                <Form.Label>Bio:</Form.Label>
                <Form.Textarea
                  type="text"
                  placeholder="Bio"
                  max-length="50"
                  value={this.userProfile.bios}
                  onChange={(e) => (this.userProfile.bios = e.currentTarget.value)}
                  rows={2}
                ></Form.Textarea>
              </Column>
            </Row>
            <p />
            <Row>
              <Button.Success
                onClick={() => {
                  userService.editUser(
                    this.userProfile.profilePicture,
                    this.userProfile.bios,
                    this.props.match.params.username
                  );
                  history.push('/users/' + this.props.match.params.username);
                }}
              >
                {' '}
                Save Changes
              </Button.Success>
            </Row>
          </Card>
        </div>
      </>
    );
  }

  mounted() {
    userService
      .getUser(this.props.match.params.username)
      .then((user) => (this.userProfile = user))
      .catch((error) => Alert.danger('Something went wrong; ' + error.message));
  }
}
