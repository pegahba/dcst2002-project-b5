import * as React from 'react';
import { Component } from 'react-simplified';
import { Alert, Card, Row, Column, Button } from '../widgets';
import { NavLink } from 'react-router-dom';
import { Game } from '../services/game-services';
import searchService from '../services/search-services';
import genreService, { Genre } from '../services/genre-services';
import platformService, { Platform } from '../services/platform-services';
import { createHashHistory } from 'history';

const history = createHashHistory();

export class SearchPage extends Component<{ match: { params: { search: string } } }> {
  games: Game[] = [];
  genres: Genre[] = [];
  platforms: Platform[] = [];

  render() {
    return (
      <>
        <div id="oneGamePage">
          <div id="pageTitle">
            <h5>{"Search results for '" + this.props.match.params.search + "'"}</h5>
          </div>
          {this.games.map((game) => (
            <Card title="">
              <Card
                key={game.game_id}
                title={<NavLink to={'/games/' + game.game_id}>{game.title}</NavLink>}
              >
                <Row>
                  <Column width={1}>
                    <img width="100%" src={game.photo}></img>
                  </Column>
                  <Column>
                    {this.genres
                      .filter((genre) => genre.game_id == game.game_id)
                      .map((g) => (
                        <Row>{g.genre}</Row>
                      ))}
                    <p />
                    {this.platforms
                      .filter((platform) => platform.game_id == game.game_id)
                      .map((p) => (
                        <Row>{p.platform}</Row>
                      ))}
                  </Column>
                </Row>
              </Card>
            </Card>
          ))}
          <p />
          <Row>
            <Button.Secondary onClick={() => history.push('/games/new')}>
              Can't find the game you're looking for? Add it!
            </Button.Secondary>
          </Row>
        </div>
      </>
    );
  }

  mounted() {
    searchService
      .getGame(this.props.match.params.search)
      .then((games) => (this.games = games))
      .catch((error) => Alert.danger('Error getting game: ' + error.message));

    genreService
      .getAllGenres()
      .then((genres) => (this.genres = genres))
      .catch((error) => Alert.danger('Something went wrong; ' + error.message));

    platformService
      .getAllPlatforms()
      .then((platforms) => (this.platforms = platforms))
      .catch((error) => Alert.danger('Something went wrong; ' + error.message));
  }
}
