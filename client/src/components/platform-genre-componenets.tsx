import * as React from 'react';
import { Component } from 'react-simplified';
import { NavLink } from 'react-router-dom';
import { Game } from '../services/game-services';
import genreService from '../services/genre-services';
import platformService from '../services/platform-services';
import { createHashHistory } from 'history';
import { Alert } from '../widgets';

const history = createHashHistory();

export class PlatformList extends Component<{ match: { params: { platform: string } } }> {
  games: Game[] = [];

  render() {
    return (
      <>
        <h3 id="pageTitle">Platform: {this.props.match.params.platform}</h3>
        <div id="gamePage">
          {this.games.map((game) => (
            <div id="flex-items">
              <NavLink to={'/games/' + game.game_id}>
                <img width="100%" src={game.photo}></img>
              </NavLink>
            </div>
          ))}
        </div>
      </>
    );
  }

  mounted() {
    platformService
      .getPlatformList(this.props.match.params.platform)
      .then((games) => (this.games = games))
      .catch((error) => Alert.danger('Something went wrong; ' + error.message));
  }
}

export class GenreList extends Component<{ match: { params: { genre: string } } }> {
  games: Game[] = [];

  render() {
    return (
      <>
        <h3 id="pageTitle">Genre: {this.props.match.params.genre}</h3>
        <div id="gamePage">
          {this.games.map((game) => (
            <div id="flex-items">
              <NavLink to={'/games/' + game.game_id}>
                <img width="100%" src={game.photo}></img>
              </NavLink>
            </div>
          ))}
        </div>
      </>
    );
  }

  mounted() {
    genreService
      .getGenreList(this.props.match.params.genre)
      .then((games) => (this.games = games))
      .catch((error) => Alert.danger('Something went wrong; ' + error.message));
  }
}
