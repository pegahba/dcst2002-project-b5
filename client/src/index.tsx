import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { HashRouter, Route } from 'react-router-dom';
import { NavBar, Alert, Button } from './widgets';
import { GameList, GamePage, GameNew, GameTop } from './components/game-components';
import { PlatformList, GenreList } from './components/platform-genre-componenets';
import { ReviewEdit, UserEdit, UserLogIn, UserNew, UserPage } from './components/user-components';
import { SearchPage } from './components/search-components';
import axios from 'axios';
import { createHashHistory } from 'history';
import { KeyboardEvent } from 'react';

const history = createHashHistory();

class Menu extends Component {
  search: string = '';
  page: number = 0;

  render() {
    return (
      <div className="TopBar">
        <NavBar brand="Home">
          <NavBar.Link to={'/all/' + this.page}>All Games</NavBar.Link>
        </NavBar>
        <div className="search">
          <input
            className="searchInput"
            type="search"
            placeholder="Search"
            onChange={(event) => {
              this.search = event.currentTarget.value;
            }}
            onKeyUp={(e: KeyboardEvent<HTMLInputElement>) => {
              e.key == 'Enter' ? history.push('/search/' + this.search) : '';
            }}
          ></input>{' '}
          <Button.Secondary
            onClick={() => {
              history.push('/search/' + this.search);
            }}
          >
            {' '}
            search
          </Button.Secondary>
        </div>

        <NavBar.Link to="/login">Account</NavBar.Link>
      </div>
    );
  }
}

const root = document.getElementById('root');
if (root)
  ReactDOM.render(
    <HashRouter>
      <div>
        <Alert />
        <Menu />
        <Route exact path="/" component={GameTop} />
        <Route exact path="/all/:page" component={GameList} />
        <Route exact path="/games/:id(\d+)" component={GamePage} />
        <Route exact path="/genres/:genre" component={GenreList} />
        <Route exact path="/platforms/:platform" component={PlatformList} />
        <Route exact path="/games/new" component={GameNew} />
        <Route exact path="/login" component={UserLogIn} />
        <Route exact path="/search/:search" component={SearchPage} />
        <Route exact path="/users/:username" component={UserPage} />
        <Route exact path="/review/:review_id/edit" component={ReviewEdit} />
        <Route exact path="/users/add/new" component={UserNew} />
        <Route exact path="/users/:username/edit" component={UserEdit} />
      </div>
    </HashRouter>,
    root
  );
