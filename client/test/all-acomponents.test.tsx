import * as React from 'react';
import { GameList, GameNew, GamePage, GameTop } from '../src/components/game-components';
import { PlatformList, GenreList } from '../src/components/platform-genre-componenets';
import { shallow } from 'enzyme';
import { Form, Button, Column, Card, Row } from '../src/widgets';
import {
  ReviewEdit,
  UserEdit,
  UserLogIn,
  UserNew,
  UserPage,
} from '../src/components/user-components';
import { SearchPage } from '../src/components/search-components';

jest.mock('../src/services/game-services', () => {
  class GameService {
    get() {
      return Promise.resolve([
        { game_id: 1, title: 'OW', release_year: 2019, photo: 'hi', description: 'bye' },
      ]);
    }

    getGamePages() {
      return Promise.resolve([
        { game_id: 1, title: 'OW', release_year: 2019, photo: 'hi', description: 'bye' },
        { game_id: 2, title: 'LOL', release_year: 2017, photo: 'hi', description: 'bye' },
        { game_id: 3, title: 'WOW', release_year: 2015, photo: 'hi', description: 'bye' },
      ]);
    }

    getMostReviews() {
      return Promise.resolve([
        { game_id: 1, title: 'OW', release_year: 2019, photo: 'hi', description: 'bye' },
      ]);
    }

    getTopRated() {
      return Promise.resolve([
        { game_id: 2, title: 'LOL', release_year: 2017, photo: 'hi', description: 'bye' },
      ]);
    }

    getLastReviewed() {
      return Promise.resolve([
        { game_id: 2, title: 'LOL', release_year: 2017, photo: 'hi', description: 'bye' },
      ]);
    }

    addGame() {
      return Promise.resolve(5);
    }
  }

  return new GameService();
});

jest.mock('../src/services/genre-services', () => {
  class GenreService {
    getGenres() {
      return Promise.resolve([
        { game_id: 1, genre: 'Action' },
        { game_id: 1, genre: 'Battle Royale' },
        { game_id: 1, genre: 'FPS' },
      ]);
    }

    getGenreList() {
      return Promise.resolve([
        {
          game_id: 1,
          title: 'OW',
          release_year: 2019,
          photo: 'hi',
          description: 'bye',
          genre: 'Action',
        },
      ]);
    }

    getAllGenres() {
      return Promise.resolve([{ game_id: 1, genre: 'Action, Battle Royale, FPS' }]);
    }

    addGenres() {
      return Promise.resolve();
    }
  }
  return new GenreService();
});

jest.mock('../src/services/platform-services', () => {
  class PlatformService {
    getPlatforms() {
      return Promise.resolve([
        { game_id: 1, platform: 'Nintendo Switch' },
        { game_id: 1, platform: 'PC' },
        { game_id: 1, platform: 'PlayStation 4' },
        { game_id: 1, platform: 'Xbox One' },
      ]);
    }

    getPlatformList() {
      return Promise.resolve([
        {
          game_id: 1,
          title: 'OW',
          release_year: 2019,
          photo: 'hi',
          description: 'bye',
          platform: 'Nintendo Switch',
        },
      ]);
    }

    getAllPlatforms() {
      return Promise.resolve([
        { game_id: 1, plaform: 'Nintendo Switch, PC, PlayStation 4, Xbox One' },
      ]);
    }

    addPlatforms() {
      return Promise.resolve();
    }
  }
  return new PlatformService();
});

jest.mock('../src/services/review-services', () => {
  class ReviewService {
    getReviews() {
      return Promise.resolve([
        { review_id: 1, review: 'is ok', username: 'user1', game_rating: 3 },
        { review_id: 2, review: 'is good', username: 'user2', game_rating: 5 },
      ]);
    }

    getReview() {
      return Promise.resolve([
        { review_id: 2, review: 'is good', username: 'user2', game_rating: 5 },
      ]);
    }

    getRating() {
      return Promise.resolve([{ avg_rating: 3 }]);
    }

    rateReview() {
      return Promise.resolve();
    }

    addReview() {
      return Promise.resolve(3);
    }

    deleteReview() {
      return Promise.resolve();
    }

    updateReview() {
      return Promise.resolve();
    }
  }
  return new ReviewService();
});

jest.mock('../src/services/user-services', () => {
  class UserService {
    logIn() {
      return Promise.resolve({ username: 'user1' });
    }

    getUserReviews() {
      return Promise.resolve({ review_id: 1, review: 'is ok', username: 'user1', game_rating: 3 });
    }

    addUser() {
      return Promise.resolve();
    }

    getUser() {
      return Promise.resolve({ profilePicture: 'pic', bios: 'bio' });
    }

    editUser() {
      return Promise.resolve();
    }
  }
  return new UserService();
});

jest.mock('../src/services/search-services', () => {
  class SearchService {
    getGame() {
      return Promise.resolve({ title: 'Searched' });
    }
  }
  return new SearchService();
});

// button location tests
describe('game component button and location tests', () => {
  test('GamePage sets location correctly on login button from review', (done) => {
    const wrapper = shallow(<GamePage match={{ params: { id: 1 } }} />);

    expect(
      wrapper.containsMatchingElement([
        <Card title="OW">
          Action, Battle Royale, FPS
          <Row>
            <Column>
              <Card title="Average Rating: 0/5">
                <p />
                Available on: Nintendo Switch, PC, PlayStation 4, Xbox One
              </Card>
            </Column>
            <Column right={true} width={2}>
              <img src="" width="100%" />
            </Column>
          </Row>
        </Card>,
        <Card title="User Reviews">
          <Card title="Have you played this game? Tell others what you liked or disliked about it!">
            <Button.Info onClick={() => {}}>Log in to review this game</Button.Info>
          </Card>
        </Card>,
      ])
    ).toEqual(true);

    wrapper.find(Button.Info).simulate('click');
    setTimeout(() => {
      expect(location.hash).toEqual('#/login');
      done();
    });
  });

  test('GameList sets location correctly on next', (done) => {
    const wrapper = shallow(<GameList match={{ params: { page: 0 } }} />);

    wrapper.find(Button.Light).at(0).simulate('click');
    setTimeout(() => {
      expect(location.hash).toEqual('#/login');
      done();
    });
  });

  test('GameNew sets location correctly on submit', (done) => {
    const wrapper = shallow(<GameNew />);

    wrapper
      .find(Form.Input)
      .at(0)
      .simulate('change', { currentTarget: { value: 'New title' } });
    wrapper
      .find(Form.Input)
      .at(1)
      .simulate('change', { currentTarget: { value: '2021' } });
    wrapper
      .find(Form.Input)
      .at(2)
      .simulate('change', { currentTarget: { value: 'g1' } });
    wrapper
      .find(Form.Input)
      .at(3)
      .simulate('change', { currentTarget: { value: 'g2' } });
    wrapper
      .find(Form.Input)
      .at(4)
      .simulate('change', { currentTarget: { value: 'g3' } });
    wrapper
      .find(Form.Input)
      .at(5)
      .simulate('change', { currentTarget: { value: 'p1' } });
    wrapper
      .find(Form.Input)
      .at(6)
      .simulate('change', { currentTarget: { value: 'p2' } });
    wrapper
      .find(Form.Input)
      .at(7)
      .simulate('change', { currentTarget: { value: 'p3' } });
    wrapper.find(Form.Textarea).simulate('change', { currentTarget: { value: 'New Description' } });
    wrapper
      .find(Form.Input)
      .at(8)
      .simulate('change', { currentTarget: { value: 'New Photo' } });

    expect(
      wrapper.containsMatchingElement(
        // @ts-ignore
        <Form.Input value="New title" />,
        // @ts-ignore
        <Form.Input value="2021" />,
        // @ts-ignore
        <Form.Input value="g1" />,
        // @ts-ignore
        <Form.Input value="g2" />,
        // @ts-ignore
        <Form.Input value="g3" />,
        // @ts-ignore
        <Form.Input value="p1" />,
        // @ts-ignore
        <Form.Input value="p2" />,
        // @ts-ignore
        <Form.Input value="p3" />,
        // @ts-ignore
        <Form.Input value="New Description" />,
        // @ts-ignore
        <Form.Input value="New Photo" />
      )
    ).toEqual(true);

    wrapper.find(Button.Success).simulate('click');
    setTimeout(() => {
      expect(location.hash).toEqual('#/games/5');
      done();
    });
  });
});

describe('user components button and location tests', () => {
  test('Userlogin sets location correctly on create new user', (done) => {
    const wrapper = shallow(<UserLogIn />);

    wrapper.find(Button.Secondary).simulate('click');
    setTimeout(() => {
      expect(location.hash).toEqual('#/users/add/new');
      done();
    });
  });

  test('Userlogin sets location correctly on login', (done) => {
    const wrapper = shallow(<UserLogIn />);

    wrapper
      .find(Form.Input)
      .at(0)
      .simulate('change', { currentTarget: { value: 'user1' } });
    wrapper
      .find(Form.Input)
      .at(1)
      .simulate('change', { currentTarget: { value: 'user1' } });

    expect(
      wrapper.containsMatchingElement(
        // @ts-ignore
        <Form.Input value="user1" />,
        // @ts-ignore
        <Form.Input value="user1" />
      )
    ).toEqual(true);

    wrapper.find(Button.Success).simulate('click');
    setTimeout(() => {
      expect(location.hash).toEqual('#/users/user1');
      done();
    });
  });

  test('UserNew sets location correctly on user creation', (done) => {
    const wrapper = shallow(<UserNew />);

    wrapper.find(Button.Success).simulate('click');
    setTimeout(() => {
      expect(location.hash).toEqual('#/login');
      done();
    });
  });

  test('UserEdit sets location correctly on save', (done) => {
    const wrapper = shallow(<UserEdit match={{ params: { username: 'user1' } }} />);

    wrapper.find(Button.Success).simulate('click');
    setTimeout(() => {
      expect(location.hash).toEqual('#/users/user1');
      done();
    });
  });
});

describe('search component button and location tests', () => {
  test('SearchPage sets location correctly on add new game', (done) => {
    const wrapper = shallow(<SearchPage match={{ params: { search: 'Searched' } }} />);

    wrapper.find(Button.Secondary).simulate('click');
    setTimeout(() => {
      expect(location.hash).toEqual('#/games/new');
      done();
    });
  });
});

//snapshots
describe('Game component tests using snapshots: ', () => {
  test('GameList', () => {
    const wrapper = shallow(<GameList match={{ params: { page: 0 } }} />);
    expect(wrapper).toMatchSnapshot();
  });

  test('GamePage', () => {
    const wrapper = shallow(<GamePage match={{ params: { id: 1 } }} />);
    expect(wrapper).toMatchSnapshot();
  });

  test('GameNew', () => {
    const wrapper = shallow(<GameNew />);
    expect(wrapper).toMatchSnapshot();
  });

  test('GameTop', () => {
    const wrapper = shallow(<GameTop />);
    expect(wrapper).toMatchSnapshot();
  });
});

describe('Genre and Platform componenet tests with snapshot', () => {
  test('GenreList', () => {
    const wrapper = shallow(<GenreList match={{ params: { genre: 'Action' } }} />);
    expect(wrapper).toMatchSnapshot();
  });

  test('PlatformList', () => {
    const wrapper = shallow(<PlatformList match={{ params: { platform: 'PC' } }} />);
    expect(wrapper).toMatchSnapshot();
  });
});

describe('User componenet tests using snapshot', () => {
  test('Userlogin', () => {
    const wrapper = shallow(<UserLogIn />);
    expect(wrapper).toMatchSnapshot();
  });

  test('UserEdit', () => {
    const wrapper = shallow(<UserEdit match={{ params: { username: 'user' } }} />);
    expect(wrapper).toMatchSnapshot();
  });

  test('UserPage', () => {
    const wrapper = shallow(<UserPage match={{ params: { username: 'user1' } }} />);
    expect(wrapper).toMatchSnapshot();
  });

  test('UserNew', () => {
    const wrapper = shallow(<UserNew />);
    expect(wrapper).toMatchSnapshot();
  });

  test('ReviewEdit', () => {
    const wrapper = shallow(<ReviewEdit match={{ params: { review_id: 1 } }} />);
    expect(wrapper).toMatchSnapshot();
  });
});

describe('Search componenet test using snapshot', () => {
  test('SearchPage', () => {
    const wrapper = shallow(<SearchPage match={{ params: { search: 'Searched' } }} />);
    expect(wrapper).toMatchSnapshot();
  });
});
